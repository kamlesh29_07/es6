<h1 align = "center">ES6 </h1>

1. ## Introduction
    
    **ES6** stands for ECMAScript 6. ECMAScript was created to standardize JavaScript, and ES6 is the 6th version of ECMAScript, it was published in 2015, and is also known as ECMAScript 2015. ECMAScript 2015 was the second major revision to JavaScript.

    **ECMAScript**, or ES, is a standardized version of JavaScript. Because all major browsers follow this specification, the terms ECMAScript and JavaScript are interchangeable.

1. ## Data Types in ES6

    1. ### let 
        * **let** keyword was introduced in ES6 to solve this potential issue with the var keyword. when we use *let*, a variable with the same name can only be declared once.
        
            **Code Samples**
            ```javascript
                let a = 5;
                let b = 10;
                console.log(a*b);//50
            ```
        * **Differences Between the var and let Keywords**

            One of the biggest problems with declaring variables with the var keyword is that you can overwrite variable declarations without an error.
            ```javascript
            var camper = 'James';
            var camper = 'David';
            console.log(camper);
            // Here the console will display the string David.
            ```

    1. ### const
        * **const** has all the feature that **let** has, with the added bonus that variables declared using const are read-only. They are a constant value, which means that once a variable is assigned with const, it cannot be reassigned.
        

            **Code Samples**
            ```javascript
                const str1 = "Java";
                const str2 = "Script";
                console.log(str1+str2);//JavaScript
            ```
            ```javascript
                const pet = "Dog";
                pet = "Cat";
                //display an error due to reassigning the value of pet;
            ```

1. ## Some Code Samples 

            
    * **for primitive type**


        ```javascript
            //Integers

            const a = 50
            console.log(a); // Integer
        ```

        ```javascript
            //boolean

            const bl = true;
            console.log(bl); // true;
        ```

        ```javascript
            //strings

            const str = "JavaScript";
            console.log(str);//JavaScript
        ```
    * **For Compound type**

        ```javascript
            //Arrays
            
            const array = [1,3,2,9,4];
            console.log(array[1]);//3

        ```
        ```javascript
            //objects

            const obj = {
                name: "Mark",
                value: 21
            };
            console.log(obj.name);//Mark
            console.log(obj.value);//21
        ```      
        
1. ## Refrence Link
    * [https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/#es6](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/#es6)
